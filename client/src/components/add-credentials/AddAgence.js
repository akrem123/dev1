import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import TextFieldGroup from '../common/TextFieldGroup';

import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addAgence } from '../../actions/profileActions';

class AddAgence extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nom: '',
      adresse: '',
      region: '',
      
      errors: {},
      disabled: false
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const ageData = {
      nom: this.state.nom,
      adresse: this.state.adresse,
      region: this.state.region,
      
    };

    this.props.addAgence(ageData, this.props.history);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  

  render() {
    const { errors } = this.state;

    return (
      <div className="add-education">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <Link to="/dashboard" className="btn btn-light">
                Go Back
              </Link>
              <h1 className="display-4 text-center">Add Agence</h1>
              <p className="lead text-center">
                Add Name Of Agency
              </p>
              <small className="d-block pb-3">* = required fields</small>
              <form onSubmit={this.onSubmit}>
                <TextFieldGroup
                  placeholder="* nom"
                  name="nom"
                  value={this.state.nom}
                  onChange={this.onChange}
                  error={errors.nom}
                />
                <TextFieldGroup
                  placeholder="* adresse "
                  name="adresse"
                  value={this.state.adresse}
                  onChange={this.onChange}
                  error={errors.adresse}
                />
                
                <TextFieldGroup
                  placeholder="* region "
                  name="region"
                  value={this.state.region}
                  onChange={this.onChange}
                  error={errors.region}
                />
                
                <input
                  type="submit"
                  value="Submit"
                  className="btn btn-info btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AddAgence.propTypes = {
  addAgence: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  errors: state.errors
});

export default connect(mapStateToProps, { addAgence })(
  withRouter(AddAgence)
);
