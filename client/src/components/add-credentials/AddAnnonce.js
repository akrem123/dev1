import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import TextFieldGroup from '../common/TextFieldGroup';
import TextAreaFieldGroup from '../common/TextAreaFieldGroup';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addAnnonce } from '../../actions/profileActions';

class AddAnnonce extends Component {
  constructor(props) {
    super(props);
    this.state = {
  
      title: '',
      prix: '',
      adressebien: '',
      typeannonce: '',
      status: '',
      surface: '',
      description: '',
      errors: {},
      disabled: false
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const annData = {
  
      title: this.state.title,
      prix: this.state.prix,
      adressebien: this.state.adressebien,
      typeannonce: this.state.typeannonce,
      status: this.state.status,
      surface: this.state.surface,
      description: this.state.description
    };

    this.props.addAnnonce(annData, this.props.history);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  

  render() {
    const { errors } = this.state;

    return (
      <div className="add-annonce">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <Link to="/dashboard" className="btn btn-light">
                Go Back
              </Link>
              <h1 className="display-4 text-center">Add Annonce</h1>
              <p className="lead text-center">
                Add Annonce
              </p>
              <small className="d-block pb-3">* = required fields</small>
              <form onSubmit={this.onSubmit}>
                <TextFieldGroup
                  placeholder="* Title"
                  name="title"
                  value={this.state.title}
                  onChange={this.onChange}
                  error={errors.title}
                />
                <TextFieldGroup
                  placeholder="* Prix"
                  name="prix"
                  value={this.state.prix}
                  onChange={this.onChange}
                  error={errors.prix}
                />
                <TextFieldGroup
                  placeholder="Adresse"
                  name="adressebien"
                  value={this.state.adressebien}
                  onChange={this.onChange}
                  error={errors.adressebien}
                  info="Tell us about the Adresse"
                />
                <TextFieldGroup
                  placeholder="Type"
                  name="typeannonce"
                  value={this.state.typeannonce}
                  onChange={this.onChange}
                  error={errors.typeannonce}
                  info="Tell us about the the Type"
                />

                <TextFieldGroup
                  placeholder="* Status"
                  name="status"
                  value={this.state.status}
                  onChange={this.onChange}
                  error={errors.status}
                />
                
                <TextFieldGroup
                  placeholder="Surface"
                  name="surface"
                  value={this.state.surface}
                  onChange={this.onChange}
                  error={errors.surface}
                  info="Tell us about the Surface"
                />
                
                <TextAreaFieldGroup
                  placeholder=" Description"
                  name="description"
                  value={this.state.description}
                  onChange={this.onChange}
                  error={errors.description}
                  info="Tell us about the the Annonce"
                />
                <input
                  type="submit"
                  value="Submit"
                  className="btn btn-info btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AddAnnonce.propTypes = {
  addAnnonce: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  errors: state.errors
});

export default connect(mapStateToProps, { addAnnonce })(
  withRouter(AddAnnonce)
);
