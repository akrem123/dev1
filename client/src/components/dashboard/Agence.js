import React, { Component } from "react";
import { connect } from "react-redux";
import propTypes from "prop-types";
import { deleteAgence } from "../../actions/profileActions";
import SingleAgence from "./SingleAgence";

class Agence extends Component {
  onDeleteClick = id => {
    this.props.deleteAgence(id);
  };

  onEditClick = id => {
    this.props.editAgence(id);
  };

  render() {
    const agence = this.props.agence
      .sort(function(a, b) {
        return +new Date(a.from) - +new Date(b.from);
      })
      .reverse()
      .map(age => (
        <SingleAgence
          age={age}
          key={age._id}
          onDeleteClick={this.onDeleteClick}
        />
      ));

    return (
      <div>
        <h4 className="mb-5 center-mobile">Agence Credentials</h4>
        <div className="evenly-space bold hide-when-tablet">
          <div className="space center">Nom</div>
          <div className="space center">Adresse</div>
          <div className="space center">Region</div>
          
        </div>
        {agence}
      </div>
    );
  }
}

deleteAgence.propTypes = {
  deleteAgence: propTypes.func.isRequired
};

export default connect(
  null,
  { deleteAgence }
)(Agence);
