import React from 'react';
import { Link } from 'react-router-dom';

const ProfileActions = () => {
  return (
    <div className="btn-group mb-4" role="group">
      <Link to="/edit-profile" className="btn btn-light">
        <i className="fas fa-user-circle text-info mr-1" /> Edit Profile
      </Link>
      <Link to="/add-annonce" className="btn btn-light">
        <i className="fab fa-black-tie text-info mr-1" />
        Add Annonce
      </Link>
      <Link to="/add-agence" className="btn btn-light">
        <i className="fas fa-graduation-cap text-info mr-1" />
        Add Agence
      </Link>
    </div>
  );
};

export default ProfileActions;
