import React, { Component, Fragment } from "react";
import moment from "moment";
import styled from "styled-components";
import { connect } from "react-redux";

import TextFieldGroup from "../common/TextFieldGroup";
import TextAreaFieldGroup from "../common/TextAreaFieldGroup";
import { withRouter } from "react-router-dom";
import {
  updateAnnonce,
  getCurrentProfile
} from "../../actions/profileActions";
import propTypes from "prop-types";

const Form = styled.form`
  margin-bottom: 1rem;
  margin-top: 1rem;
  border-top: 1px solid #6c757d;
  padding-top: 1rem;
`;

const AnnonceDetails = styled.div`
  margin-bottom: 1rem;
`;

class SingleAnnonce extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      prix: "",
      adressebien: "",
      typeannonce: "",
      status: "",
      surface: "",
      description: "",

      errors: {},
      disabled: false,
      editMode: false,

      
      titleEdit: "",
      prixEdit: "",
      adressebienEdit: "",
      typeannonceEdit: "",
      statusEdit: "",
      surfaceEdit: "",
      descriptionEdit: "",
      
      id: 0
    };
  }

  componentDidMount() {
    this.setState({
      title: this.props.ann.title,
      prix: this.props.ann.prix,
      adressebien: this.props.ann.adressebien,
      typeannonce: this.props.ann.typeannonce,
      status: this.props.ann.status,
      surface: this.props.ann.surface,
      description: this.props.ann.description,
      id: this.props.ann._id
    });
  }

  onDeleteClick = id => {
    this.props.onDeleteClick(id);
  };

  onEditClick = id => {
    this.setState({
      editMode: !this.state.editMode,
      titleEdit: this.state.title,
      prixEdit: this.state.prix,
      adressebienEdit: this.state.adressebien,
      typeannonceEdit: this.state.typeannonce,
      statusEdit: this.state.status,
      surfaceEdit: this.state.surface,
      descriptionEdit: this.state.description
    });
  };
  onCancelClick = id => {
    this.setState({
      editMode: !this.state.editMode,
      title: this.state.titleEdit,
      prix: this.state.prixEdit,
      adressebien: this.state.adressebienEdit,
      typeannonce: this.state.typeannonceEdit,
      status: this.state.statusEdit,
      surface: this.state.surfaceEdit,
      description: this.state.descriptionEdit
    });
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  

  onSubmit = e => {
    e.preventDefault();
  };

  onUpdate = id => {
    const { ann, updateAnnonce } = this.props;

    this.setState({
      editMode: !this.state.editMode,
      
      titleEdit: this.state.title,
      prixEdit: this.state.prix,
      adressebienEdit: this.state.adressebien,
      typeannonceEdit: this.state.typeannonce,
      statusEdit: this.state.status,
      surfaceEdit: this.state.surface,
      descriptionEdit: this.state.description,
      errors: {}
    });

    const annData = {
      
      title: this.state.title,
      prix: this.state.prix,
      adressebien: this.state.adressebien,
      typeannonce: this.state.typeannonce,
      status: this.state.status,
      surface: this.state.surface,
      description: this.state.description,
      id: id
    };

    updateAnnonce(annData, ann._id);
  };

  componentWillReceiveProps(nextProps) {
    if (typeof nextProps.errors[nextProps.ann._id] !== "undefined") {
      if (Object.keys(nextProps.errors[nextProps.ann._id]).length === 0) {
        this.setState({
          editMode: false
        });
      } else {
        this.setState({
          editMode: true
        });
        this.setState({
          errors: {
            
            title: nextProps.errors[nextProps.ann._id].title,
            prix: nextProps.errors[nextProps.ann._id].prix,
            adressebien: nextProps.errors[nextProps.ann._id].adressebien,
            typeannonce: nextProps.errors[nextProps.ann._id].typeannonce,
            status: nextProps.errors[nextProps.ann._id].status,
            surface: nextProps.errors[nextProps.ann._id].surface,
            description: nextProps.errors[nextProps.ann._id].description,
            id: nextProps.errors[nextProps.ann._id].id
          }
        });
      }
    }
  }

  render() {
    const { ann } = this.props;
    const { editMode, errors } = this.state;

    const actionButtons = (
      <div className="space center">
        {!editMode ? (
          <Fragment>
            <button
              className="btn btn-info"
              onClick={this.onEditClick.bind(this, ann._id)}
            >
              Edit
            </button>{" "}
            <button
              className="btn btn-danger"
              onClick={this.onDeleteClick.bind(this, ann._id)}
            >
              Delete
            </button>
          </Fragment>
        ) : (
          <Fragment>
            <button
              className="btn btn-info"
              onClick={this.onUpdate.bind(this, ann._id)}
            >
              Submit
            </button>
            <button
              className="btn btn-danger"
              onClick={this.onCancelClick.bind(this, ann._id)}
            >
              Cancel
            </button>
          </Fragment>
        )}
      </div>
    );

    return (
      <Form onSubmit={this.onSubmit}>
        <AnnonceDetails className="evenly-space column-tablet">
          
          <div className="space center">
            <div className="bold show-tablet">Title</div>
            {!editMode ? (
              this.state.title
            ) : (
              <TextFieldGroup
                placeholder="* Annonce Title"
                name="title"
                value={this.state.title}
                onChange={this.onChange}
                error={errors.title}
              />
            )}
          </div>
          
          <div className="space center">
            <div className="bold show-tablet">prix</div>
            {!editMode ? (
              this.state.prix
            ) : (
              <TextFieldGroup
                placeholder="* Prix"
                name="prix"
                value={this.state.prix}
                onChange={this.onChange}
                error={errors.prix}
              />
            )}
          </div>
          <div className="space center">
            <div className="bold show-tablet">Adresse</div>
            {!editMode ? (
              this.state.adressebien
            ) : (
              <TextFieldGroup
                placeholder="Adresse"
                name="adressebien"
                value={this.state.adressebien}
                onChange={this.onChange}
                error={errors.adressebien}
              />
            )}
          </div>
          <div className="space center">
            <div className="bold show-tablet">Type</div>
            {!editMode ? (
              this.state.typeannonce
            ) : (
              <TextFieldGroup
                placeholder="Type"
                name="typeannonce"
                value={this.state.typeannonce}
                onChange={this.onChange}
                error={errors.typeannonce}
              />
            )}
          </div>
          <div className="space center">
            <div className="bold show-tablet">Status</div>
            {!editMode ? (
              this.state.status
            ) : (
              <TextFieldGroup
                placeholder="* Status"
                name="status"
                value={this.state.status}
                onChange={this.onChange}
                error={errors.status}
              />
            )}
          </div>
          <div className="space center">
            <div className="bold show-tablet">Surface</div>
            {!editMode ? (
              this.state.surface
            ) : (
              <TextFieldGroup
                placeholder="Surface"
                name="surface"
                value={this.state.surface}
                onChange={this.onChange}
                error={errors.surface}
              />
            )}
          </div>
          <div className="space form-box center">
            <div className="bold show-tablet">Description</div>
            {!editMode ? (
              this.state.description
            ) : (
              <TextAreaFieldGroup
                placeholder="Job Description"
                name="description"
                value={this.state.description}
                onChange={this.onChange}
                error={errors.description}
                info="Tell us about the position"
              />
            )}
          </div>
          <div />
        </AnnonceDetails>
        {actionButtons}
      </Form>
    );
  }
}

SingleAnnonce.proptypes = {
  updateAnnonce: propTypes.func.isRequired,
  annonce: propTypes.object.isRequired,
  errors: propTypes.object.isRequired
};

const mapStateToProps = state => ({
  annonce: state.ann,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { updateAnnonce, getCurrentProfile }
)(withRouter(SingleAnnonce));
