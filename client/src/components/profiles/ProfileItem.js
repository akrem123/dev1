import React, { Component } from "react";
import propTypes from "prop-types";
import { Link } from "react-router-dom";
import isEmpty from "../../validation/is-empty";

class ProfileItem extends Component {
  render() {
    const { profile } = this.props;

    return (
      <div className="card card-body bg-light mb-3">
        <div className="row">
          <div className="col-2">
            <Link to={`/profile/${profile.handle}`}>
              <img
                src={profile.user.avatar}
                alt=""
                className="rounded-circle"
              />
            </Link>
          </div>
          <div className="col-lg-6 col-md-4 col-8">
            <h3>{profile.user.name}</h3>
            <p>
              {profile.poste}{" "}
              {isEmpty(profile.agence.nom) ? null : (
                <span>at {profile.agence.nom}</span>
              )}
            </p>
            <p>
              {isEmpty(profile.tel) ? null : (
                <span>{profile.tel}</span>
              )}
            </p>
            <Link to={`/profile/${profile.handle}`} className="btn btn-info">
              View Profile
            </Link>
          </div>
          
        </div>
      </div>
    );
  }
}

ProfileItem.propTypes = {
  profile: propTypes.object.isRequired
};

export default ProfileItem;
