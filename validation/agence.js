const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateAgenceInput(data) {
  let errors = {};

  data.nom = !isEmpty(data.nom) ? data.nom : '';
  data.adresse= !isEmpty(data.adresse) ? data.adresse : '';
  data.region = !isEmpty(data.region) ? data.region : '';
  

  if (Validator.isEmpty(data.nom)) {
    errors.nom = 'Nom field is required';
  }

  if (Validator.isEmpty(data.adresse)) {
    errors.adresse = 'Adresse field is required';
  }

  if (Validator.isEmpty(data.region)) {
    errors.region = 'Region of Agence field is required';
  }

  

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
