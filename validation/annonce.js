const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateAnnonceInput(data) {
  let errors = {};

  data.title = !isEmpty(data.title) ? data.title : '';
  data.prix = !isEmpty(data.prix) ? data.prix : '';
  data.status = !isEmpty(data.status) ? data.status : '';

  if (Validator.isEmpty(data.title)) {
    errors.title = 'Annonce title field is required';
  }

  if (Validator.isEmpty(data.prix)) {
    errors.prix = 'prix field is required';
  }

  if (Validator.isEmpty(data.status)) {
    errors.status = 'status Annonce field is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
